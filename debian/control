Source: fpzip
Section: utils
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Alastair McKinstry <mckinstry@debian.org>
Build-Depends: debhelper-compat (= 13),
 architecture-is-64-bit,
 cmake
Standards-Version: 4.7.0
Homepage: https://github.com/LLNL/fpzip
Vcs-Browser: https://salsa.debian.org/science-team/fpzip.git
Vcs-Git: https://salsa.debian.org/science-team/fpzip.git -b debian/latest

Package: fpzip-utils
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, libfpzip1 ( = ${binary:Version})
Description: FP array compression library - tools
 fpzip is a library and command-line utility for lossless and optionally lossy
 compression of 2D and 3D floating-point arrays.  fpzip assumes spatially
 correlated scalar-valued data, such as regularly sampled continuous functions,
 and is not suitable for compressing unstructured streams of floating-point
 numbers.
 This package provides command-line utilities.

Package: libfpzip1
Section: libs
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: FP array compression library
 fpzip is a library and command-line utility for lossless and optionally lossy
 compression of 2D and 3D floating-point arrays.  fpzip assumes spatially
 correlated scalar-valued data, such as regularly sampled continuous functions,
 and is not suitable for compressing unstructured streams of floating-point
 numbers.
 This package provides the shared library.

Package: libfpzip-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, libfpzip1 (= ${binary:Version})
Multi-Arch: same
Description: Development files for the FP array compression library
 fpzip is a library and command-line utility for lossless and optionally lossy
 compression of 2D and 3D floating-point arrays.  fpzip assumes spatially
 correlated scalar-valued data, such as regularly sampled continuous functions,
 and is not suitable for compressing unstructured streams of floating-point
 numbers.
 This package provides the headers and static library.
